---
title: "Pumpkin Pilau with pumpkin tahdig crust"
date: 2021-09-14T22:13:40+01:00
draft: true
tags: ["rice"]
keywords: ["rice", "Pilau", "pumpkin", "autumn", "tahdig"]
---

###### Ingredients:
+ 2 cups rice
+ 3 cups peeled,diced pumpkin
+ peeled,thin big sliced pumpkin (for covering the bottom of the pot)
+ 1 large onions, chopped
+ 3 tbsp dried Barberries
+ 3 tbsp flaked ​​almonds
+ 3 tbsp flaked pistachios
+ 4 tbsp sugar
+ 1 tsp cinnamon
+ 1/2 tsp cardamom powder
+ 1 tsp turmeric
+ 4 tbsp Saffron
+ Oil
+ Salt to taste

###### Instructions:

1. In a large skillet, heat the oil over medium-high heat. Add the onion, cook and stir occasionally, until it begins to soften and a bit golden, then add the turmeric and continue to cook.

2. Add the pumpkins to the skillet. Cook, stiring for one to two minutes. Toss them in the saffron, cardamom, cinnamon, 2 tbsp sugar and 1/2 tsp salt. Turn off the heat and put the skillet aside.

3. Pour some water in a suitable non-stick pot with a tight-fitting lid,and put it over high heat until it boils. Wash the rice and when the water boils, add the rice and salt to the pot and wait until it is ready to rinse. The par-cooked rice is ready when it is soft around the edges but still firm (not crunchy) in the center.
Empty the rice into a fine mesh strainer, rinse it under cold water to halt the cooking process, and to wash off the excess salt. then rinse it.

4. Heat the oil in the empty pot over medium heat, toss the chopped pumpkin slices with 1 teaspoon sugar and 2 tablespoons saffaron. Then arrange them in the pot to have one layer of pumpkin slices that cover the bottom of the pot.

5. On top of the pumpkin slices, arrange the rice and the pumpkin mixture in alternate layers. Finish with rice. Cover the pot and let the rice steam for about 30 to 45 minutes or until steam rises, and the rice grains are tender through.

5. In the meantime, rinse the barberries under the water and then left them to drain a bit. Heat the oil in a skillet over medium heat. Add the barberries. Cook, stirring, a few seconds. Add sugar and then turn off the heat. put the skillet aside. Add flaked pistachios and flaked almonds, stiring until they are heated through.

6. Serve the pilau, garnish with the barberries and the flaked ​​almonds and pistachios. Enjoy it!


**Note:**
+ To fry barberries, use fresh oil or for the taste it is better to use unsalted-butter.
+ In order for the barberries color to remain and be shiny and not to give their color to the food, turn off the heat as soon as the sugar grains are dissolved.
+ Also, be careful not to add flaked pistachios and flaked almonds when the heat is on, because the color of pistachios and almonds changes completely by heat, and it loses its original color.
+ When you want to roast barberries, the flame temperature should be low, because if the flame is high, the barberries dry and stick to the teeth when serving food.
+ Do not put saffron on high heat because its aroma will decrease.
