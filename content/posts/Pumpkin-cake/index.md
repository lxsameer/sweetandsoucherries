---
title: "Pumpkin Cake"
date: 2021-08-28T12:11:53+01:00
draft: false
tags: ["cake"]
keywords: ["cake", "pumpkin", "autumn", ]

---

###### Ingredients:

+ 3 eggs
+ 1 cup sugar
+ 1 cup Chopped walnuts
+ 2 cups of flour
+ 3 tablespoons yogurt
+ 1 cup oil
+ 1 cup pumpkin puree
+ 1 1/2 tsb pumpkin pie spice
+ 2sb baking powder
+ 1/2 tbs vanilla extract
+ salt 1

###### For frosting:

+ 200g unsalted butter
+ 400g icing sugar
+ 3-4 tbsp milk
+ 1 tsp vanilla extract
+ food coloring(orange and green)

###### Instructions:

1. Preheated the oven to 180°C.

2. In a large bowl, Beat the eggs and the sugar for about 3-4 minutes until the mixture becomes soft and creamy, smooth and also the sugar dissolves well.

3. Now add the yogurt and oil and mix the ingredients well.

4. Then add the pumpkin puree and mix the batter again until just combined.

5. in another bowl, whisk together the flour, the baking powder, the pumpkin pie spices and salt. Add the dry ingredients to the batter in 2 to 3 steps and stir it with a spatula.

6. At this stage, you should not use a mixer at all and stirring with a spatula is enough. We just should not mix the flour and liquid too much as soon as they are mixed together.

7. Now grease a desired mold with a bit of oil. Transfer the batter to mold and put it in the oven for 30 to 45 minutes (depending on the type of oven).
