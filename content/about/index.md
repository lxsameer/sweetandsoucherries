---
title: "Mary Sali"
layout: "about"
picture: "/images/me.jpg"
subtitle: "A spouse, a web developer, a fitness trainer-to-be"
alertbar: true
---

Hello and welcome to **Sweet and Sour Cherries**. Thanks for stopping by. My name is Mary. Currently live in Ireland, in the beautiful city of Dublin.

**Know me more:**

I am not a chef, but I do like to sort out some ingredients, fix up a meal for myself and my husband.

I do not follow any diet, but I try to eat healthily and keep my balance in eating foods. I believe with a balance diet and doing daily exercise, I can have variety of foods.

It is been nearly three years that I sat my mind to start a blog post about foods and desserts. Every time I made an excuse for not doing it. At long last, I made my mind and got it out of my system and did it.

This blog is all about meal preparing and contains all kind of cookery and baking. From easy to difficult. From healthy and of course, not much healthy recipes.

As I have a sweet tooth, I would love to bake a cake and pastries from time to time and treat myself with a cup of tea or undoubtedly with a cup of black coffee.

I share with you homemade recipes in my everyday life. Hope you also enjoy them as much as I do.
