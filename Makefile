clean:
	rm -rf public
build:
	hugo -D

deploy: clean build
	rsync -vlcr --delete-after ./public/* core.sweetandsourcherries.com:/home/mary/public/

serve: build
	hugo serve
